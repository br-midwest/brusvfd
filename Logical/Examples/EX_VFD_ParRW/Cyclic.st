
PROGRAM _CYCLIC
	(* These funcition blocks require similar input, the best way to get the correct name input is to type 'VFD_' then try to auto complete
	output and input require an int of atleast size UINT for most parameters. The plcAddress and node can be found by highlighting over the powerlink in the physical view.
	
	Certain parameters can only be written to when the motor is not in 'run' state, there wil be a status telling you when this is occuring.
	 *)
	
	VFD_PARAM_READ_0(enable := TRUE , plcAddress := 'IF3', node := 1, name := nameEnum, output := ADR(output), size := SIZEOF(output));
	
	VFD_PARAM_WRITE_0(enable := TRUE , plcAddress := 'IF3', node := 1, name := VFD_IDA_CUST_CURRENT, input := ADR(input), size := SIZEOF(input));
	
	
	
END_PROGRAM
