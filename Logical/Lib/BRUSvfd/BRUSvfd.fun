
FUNCTION_BLOCK VFD_PARAM_READ (*Reads a parameter for a VFD*)
	VAR_INPUT
		enable : BOOL; (*enable var*)
		plcAddress : {REDUND_UNREPLICABLE} STRING[80]; (*String for PLC address*)
		node : USINT; (*node of powerlink*)
		name : name; (*enumertation of type 'name'*)
		output : UDINT; (*pointer to where the read number should go *)
		size : USINT; (*size of the output *)
	END_VAR
	VAR_OUTPUT
		ErrorInfo : UDINT;
	END_VAR
	VAR
		EplSDORead_0 : EplSDORead; (*INTERNAL*)
		subparIndex : USINT;
		parIndex : UINT;
		ErrorIn : UDINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK VFD_PARAM_WRITE (*Writes a parameter for a VFD*)
	VAR_INPUT
		enable : BOOL; (*enable var*)
		plcAddress : {REDUND_UNREPLICABLE} STRING[80]; (*String for PLC address*)
		node : USINT; (*node of powerlink*)
		name : name; (*enumertation of type 'name'*)
		input : UDINT; (*pointer to the input number*)
		size : USINT; (*size of the input *)
	END_VAR
	VAR_OUTPUT
		ErrorInfo : UDINT;
		status : STRING[80]; (*output of FB current mode*)
	END_VAR
	VAR
		EplSDOWrite_0 : EplSDOWrite; (*INTERNAL*)
		motorState : UDINT;
		EplSDORead_0 : EplSDORead;
		wordOut : WORD;
		previousName : name;
		notInRun : BOOL;
		parIndex : UINT;
		subparIndex : USINT;
		checkForRun : BOOL;
		writable : BOOL;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION ACPietx : BOOL (*Returns string with ACPi error text*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		LFT_Input : INT; (*LFT_Input in the ACPi I/O mapping*)
		ACPiModel : ACPi_Model_enum;
		pErrStr : UDINT; (*Address of a *)
	END_VAR
	VAR
		errStr : REFERENCE TO STRING[80];
	END_VAR
END_FUNCTION
