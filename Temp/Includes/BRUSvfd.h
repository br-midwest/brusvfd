/* Automation Studio generated header file */
/* Do not edit ! */
/* BRUSvfd  */

#ifndef _BRUSVFD_
#define _BRUSVFD_
#ifdef __cplusplus
extern "C" 
{
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
/* Datatypes and datatypes of function blocks */
typedef enum name
{	VFD_HMIS_DRIVESTATE,
	VFD_ULN_MAINVOLTAGE,
	VFD_OTR_MOTORTORQUE,
	VFD_HSP_HIGHSPEED,
	VFD_LSP_LOWSPEED,
	VFD_SFR_DRVIESWITCHFREQ,
	VFD_TFR_MAXOUTFREQ,
	VFD_INV_DRIVENOMCURRENT,
	VFD_RFR_OUTPUTFREQ,
	VFD_LCR_MOTORCURRENT,
	VFD_OPR_MOTORPOWER,
	VFD_FRH_FREQ_REF_BEFORE_RAMP,
	VFD_MMF_MEASOTUTFREQ,
	VFD_THD_DRIVETHERMSTATE,
	VFD_FRHT_FRH_HIGHRESOLUTION,
	VFD_RTH_MOTOR_RUN_TIME,
	VFD_RTH_POWER_ON_TIME,
	VFD_PTH_POWERCONSUM,
	VFD_BSP_REF_TEMPLATE,
	VFD_VDP_DRIVESOFTWARE,
	VFD_PAN0_DEVICENAME,
	VFD_PAN1_DEVICENAME,
	VFD_PAN2_DEVICENAME,
	VFD_PAN3_DEVICENAME,
	VFD_PAN4_DEVICENAME,
	VFD_PAN5_DEVICENAME,
	VFD_PAN6_DEVICENAME,
	VFD_PAN7_DEVICENAME,
	VFD_PAN8_DEVICENAME,
	VFD_PAN9_DEVICENAME,
	VFD_CFG_MACROCONFIG,
	VFD_NPR_RATEDMOTORPOWER,
	VFD_UNS_NOMMOTORVOLT,
	VFD_ETAD_DRIVECOM,
	VFD_FRS_NOMMOTORFREQ,
	VFD_NCR_NOMMOTORCURRENT,
	VFD_NSP_NOMMOTORSPEED,
	VFD_COS_MOTORCOSPHI,
	VFD_RSA_CUSTSTATOR_RESISTANCE,
	VFD_IDA_CUST_CURRENT,
	VFD_LFA_CURSTLEAK_INDUCTANCE,
	VFD_TRA_CUSTROTOR_T_CONT,
	VFD_SPG_SPEEDPROPORTIONALGAIN,
	VFD_SIT_SPEEDTIMEINTEGRAL,
	VFD_SFC_K_SPEEDLOOPFILTER,
	VFD_SPAT_ACCDELTATIME,
	VFD_SPAL_ACCDELTASPEED,
	VFD_SPDL_DECCDELTASPEED,
	VFD_SPDT_DECCDELTATIME
} name;

typedef enum ACPi_Model_enum
{	brusACPi_S44 = 0,
	brusACPi_X64 = 1,
	brusACPi_P74 = 2,
	brusACPi_P84 = 3,
	brusACPi_P66 = 10,
	brusACPi_P76 = 11,
	brusACPi_P86 = 12,
	brusACPi_P96 = 13
} ACPi_Model_enum;

typedef struct VFD_PARAM_READ
{
	/* VAR_INPUT (analog) */
	plcstring plcAddress[81];
	unsigned char node;
	enum name name;
	unsigned long output;
	unsigned char size;
	/* VAR_OUTPUT (analog) */
	unsigned long ErrorInfo;
	/* VAR (analog) */
	struct EplSDORead EplSDORead_0;
	unsigned char subparIndex;
	unsigned short parIndex;
	unsigned long ErrorIn;
	/* VAR_INPUT (digital) */
	plcbit enable;
} VFD_PARAM_READ_typ;

typedef struct VFD_PARAM_WRITE
{
	/* VAR_INPUT (analog) */
	plcstring plcAddress[81];
	unsigned char node;
	enum name name;
	unsigned long input;
	unsigned char size;
	/* VAR_OUTPUT (analog) */
	unsigned long ErrorInfo;
	plcstring status[81];
	/* VAR (analog) */
	struct EplSDOWrite EplSDOWrite_0;
	unsigned long motorState;
	struct EplSDORead EplSDORead_0;
	plcword wordOut;
	enum name previousName;
	unsigned short parIndex;
	unsigned char subparIndex;
	/* VAR_INPUT (digital) */
	plcbit enable;
	/* VAR (digital) */
	plcbit notInRun;
	plcbit checkForRun;
	plcbit writable;
} VFD_PARAM_WRITE_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void VFD_PARAM_READ(struct VFD_PARAM_READ* inst);
_BUR_PUBLIC void VFD_PARAM_WRITE(struct VFD_PARAM_WRITE* inst);
_BUR_PUBLIC plcbit ACPietx(signed short LFT_Input, enum ACPi_Model_enum ACPiModel, unsigned long pErrStr);


#ifdef __cplusplus
};
#endif
#endif /* _BRUSVFD_ */

